name := "service"
organization := "ru.kinoplan24"
version := "0.1"

scalaVersion := "2.11.8"

lazy val root = project.in(file(".")).enablePlugins(PlayScala)

libraryDependencies ++= Seq(
  specs2 % Test
)

scalacOptions += "-deprecation"
scalacOptions += "-feature"

fork            in run := true
sources         in (Compile, doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false
packageName     in Universal := "service"
