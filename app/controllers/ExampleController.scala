package controllers

import play.api.mvc.{Action, Controller}


class ExampleController extends Controller {

  def fib_tailrec(length: Int): BigInt = {
    def rec(length: Int, a: BigInt, b: BigInt): BigInt = {
      length match {
        case 0 => b
        case _ => rec(length - 1, a + b, a)
      }
    }

    rec(length, 1, 0)
  }

  def fib(length: Int) = (0 until (length - 1)) map (length => fib_tailrec(length))

  def index(length: Int) = Action {
    request => {
      Ok("Последовательность Фибоначчи: " + fib(length).mkString(","))
    }
  }

}

//
//        1836311903
//Int -   2147483647
//Long -  9223372036854775807